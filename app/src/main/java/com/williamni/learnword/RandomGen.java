package com.williamni.learnword;

import android.content.Context;

import java.util.Random;


/**
 * Created by nwb on 2/22/16.
 */
public class RandomGen extends Random{
    public char nextChar(boolean isUpcase){
        int r = nextInt(26);
        char c;
        if(isUpcase == false){
            c = (char)(r + 'a');
        }
        else{
            c = (char)(r + 'A');
        }
        return c;
    }

    public char nextChar(){
        int r = nextInt(26 * 2);
        char c;
        if( r < 26 ){
            c = (char)(r + 'a');
        }
        else{
            r = r - 26;
            c = (char)(r + 'A');
        }

        return c;
    }

    public char nextSeqChar(char c){
        if(c == 'z'){
            return 'a';
        }
        else if(c == 'Z'){
            return 'A';
        }
        else{
            return (char) (c+1);
        }
    }

    public char prevSeqChar(char c){
        if(c == 'a'){
            return 'z';
        }
        else if(c == 'A'){
            return 'Z';
        }
        else{
            return (char) (c-1);
        }
    }

    public ColorItem nextColor(Context context){
        int colorCount = Colors.getCount();
        int index = nextInt(colorCount);
        return Colors.getColor(context, index);
    }

    public int genColor(){
        int rand = nextInt(27);
        int blue = rand % 3;
        rand /= 3;
        int green = rand % 3;
        rand /= 3;
        int red = rand % 3;

        blue = getColorByIndex(blue);
        green = getColorByIndex(green);
        red = getColorByIndex(red);

        int color = 0xff000000 | (red << 16) | (green << 8) | (red);
        return color;
    }

    private int getColorByIndex(int index){
        int r = 0;
        switch (index){
            case 1:
                r = 0x80;
                break;
            case 2:
                r = 0xff;
                break;
            default:
                r = 0x0;
                break;
        }
        return r;
    }


}
