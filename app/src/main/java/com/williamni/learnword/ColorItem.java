package com.williamni.learnword;

/**
 * Created by nwb on 2/25/16.
 */
public class ColorItem {
    int color;
    String name;
    ColorItem(){
        color = 0;
        name = "";
    }
    ColorItem(int c, String n){
        color = c;
        name = n;
    }
}
