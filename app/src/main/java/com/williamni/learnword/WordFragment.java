package com.williamni.learnword;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Random;


public class WordFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    // TODO: Rename and change types of parameters
    private String mType;
    private char mChar;
    private boolean mUpcase = true;
    private RandomGen randomGen = new RandomGen();

    public WordFragment() {
        // Required empty public constructor
    }

    public static WordFragment newInstance() {
        WordFragment fragment = new WordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_word, container, false);
        final TextView textView = (TextView)view.findViewById(R.id.WordDisplay);
        final CheckBox checkBox = (CheckBox)view.findViewById(R.id.Checkbox);
        if((textView == null) || (checkBox == null)){
            return null;
        }

        mUpcase = true;
        mChar = randomGen.nextChar(mUpcase);
        updateView(view);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mUpcase = b;
                if (mUpcase) {
                    mChar = Character.toUpperCase(mChar);
                } else {
                    mChar = Character.toLowerCase(mChar);
                }
                updateView(view);
            }
        });

        Button prevBtn = (Button)view.findViewById(R.id.Previous);
        Button nextBtn = (Button)view.findViewById(R.id.Next);
        Button randomBtn = (Button)view.findViewById(R.id.Random);

        prevBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mChar = randomGen.prevSeqChar(mChar);
                updateView(view);
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mChar = randomGen.nextSeqChar(mChar);
                updateView(view);
            }
        });

        randomBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mChar = randomGen.nextChar(mUpcase);
                updateView(view);
            }
        });

        return view;
    }

    private void updateView(View view){
        TextView textView = (TextView)view.findViewById(R.id.WordDisplay);
        CheckBox checkBox = (CheckBox)view.findViewById(R.id.Checkbox);
        textView.setText(String.valueOf(mChar));
        checkBox.setChecked(mUpcase);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
