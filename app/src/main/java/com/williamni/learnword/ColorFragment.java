package com.williamni.learnword;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ColorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ColorFragment extends Fragment {
    private ColorItem mColor;
    private RandomGen randomGen = new RandomGen();

    public ColorFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ColorFragment newInstance() {
        ColorFragment fragment = new ColorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_color, container, false);

        final View colorRect = view.findViewById(R.id.ColorRect);
        if(colorRect == null){
            return null;
        }

        mColor = randomGen.nextColor(getContext());
        updateView(view);

        Button randomBtn = (Button)view.findViewById(R.id.Random);

        randomBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mColor = randomGen.nextColor(getContext());
                updateView(view);
            }
        });

        return view;
    }

    private void updateView(View view){
        View colorRect = view.findViewById(R.id.ColorRect);
        TextView colorName = (TextView) view.findViewById(R.id.ColorName);
        colorRect.setBackgroundColor(mColor.color);
        colorName.setText(mColor.name);
        colorName.setTextColor(mColor.color);
    }

}
