package com.williamni.learnword;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NumberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NumberFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private int mInt;
    private RandomGen randomGen = new RandomGen();
    private static final int MAX_INT_VAL = 101;


    public NumberFragment() {
        // Required empty public constructor
    }



    public static NumberFragment newInstance() {
        NumberFragment fragment = new NumberFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_number, container, false);
        final TextView textView = (TextView)view.findViewById(R.id.WordDisplay);
        if(textView == null){
            return null;
        }

        mInt = randomGen.nextInt(MAX_INT_VAL);
        textView.setText(String.valueOf(mInt));


        Button prevBtn = (Button)view.findViewById(R.id.Previous);
        Button nextBtn = (Button)view.findViewById(R.id.Next);
        Button randomBtn = (Button)view.findViewById(R.id.Random);

        prevBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mInt += MAX_INT_VAL - 1;
                mInt %= MAX_INT_VAL;
                textView.setText(String.valueOf(mInt));
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mInt++;
                mInt %= MAX_INT_VAL;
                textView.setText(String.valueOf(mInt));
            }
        });

        randomBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mInt = randomGen.nextInt(MAX_INT_VAL);
                textView.setText(String.valueOf(mInt));
            }
        });

        return view;
    }

}
