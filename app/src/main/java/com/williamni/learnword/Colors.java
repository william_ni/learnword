package com.williamni.learnword;

import android.content.Context;
import android.graphics.Color;

/**
 * Created by nwb on 2/24/16.
 */
public class Colors {
    public static int colors[] = {
            Color.BLACK,
            Color.WHITE,
            Color.RED,
            Color.GREEN,
            Color.BLUE,
            Color.GRAY,
            Color.CYAN,
            Color.YELLOW,
            Color.MAGENTA,
            0xFFFFA500, //orange
            0xFFA52A2A, //brown
            0xFF90EE90, //light green
            0xFF800080, //purple
            0xFFFFC0CB //pink
    };

    public static String colorNames[] = {
            "Black",
            "White",
            "Red",
            "Green",
            "Blue",
            "Gray",
            "Cyan",
            "Yellow",
            "Magenta",
            "Orange",
            "Brown",
            "LightGreen",
            "Purple",
            "Pink"
    };
    public static int getCount(){
        return colors.length;
    }

    public static ColorItem getColor(Context context, int index){
        ColorItem item = new ColorItem(colors[index], getStringResourceByName(context, colorNames[index]));
        return item;
    }

    public static String getStringResourceByName(Context context, String aString) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        if(resId == 0) return aString;
        return context.getString(resId);
    }
}
